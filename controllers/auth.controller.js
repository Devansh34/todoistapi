const db = require("../models");
const config = require("../config/auth.config");
const User = db.user;
const logger=require("../logger.js")

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");


exports.signup = (req, res) => {
  User.create({
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
  })
    .then((user) => {
      res.status(201).json({ message: 'User was registered successfully', user });
    })
    .catch((err) => {
      res.status(500).json({ message: 'Error registering user', error: err.message });
    });
};

exports.signin = (req, res) => {
  User.findOne({
    where: {
      username: req.body.username,
    },
  })
    .then((user) => {
      if (!user) {
        logger.error("User Not found")
        return res.status(404).send({ message: "User Not found. " });
      }

      var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);

      if (!passwordIsValid) {
        logger.error("Invalid Password!")
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!",
        });
      }

      const token = jwt.sign({ id: user.username }, config.secret, {
        algorithm: "HS256",
        allowInsecureKeySizes: true,
        expiresIn: 86400,
      });

      return res.status(200).send({ message: "Login Successfully", accessToken: token });
    })
    .catch((err) => {
      logger.error(err.message);
      res.status(500).send({ message: "Internal Server Error" });
    });
};