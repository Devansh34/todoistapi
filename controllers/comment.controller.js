const db = require("../models");
const Comment = db.comment;
const Task = db.task;
const Project = db.project;
const authJwt = require("../middleware/authJwt.js");
const logger = require("../logger.js");

exports.findAll = (req, res) => {
  authJwt.verifyToken(req, res, () => {
    const authenticatedUsername = req.userId;
    Comment.findAll({
      where: { username: authenticatedUsername },
    })
      .then((data) => res.send(data))
      .catch((err) => {
        console.error("Error retrieving comments:", err);
        logger.error("Internal server error while retrieving comments.")
        res.status(500).send({
          message: "Internal server error while retrieving comments.",
        });
      });
  });
};

exports.create = (req, res) => {
  authJwt.verifyToken(req, res, () => {
    const authenticatedUsername = req.userId;
    const comment = {
      task_id: req.body.task_id,
      project_id: req.body.project_id,
      content: req.body.content,
      attachment: req.body.attachment ? req.body.attachment : null,
      username:authenticatedUsername
    };

    Task.findByPk(comment.task_id)
      .then((task) => {
        if (task && task.username === authenticatedUsername) {
          return Comment.create(comment);
        } else {
          logger.error("Task not found or does not belong to the authenticated user.")
          throw new Error("Task not found or does not belong to the authenticated user.");
        }
      })
      .then((createdComment) => {
        return Task.findByPk(comment.task_id);
      })
      .then((task) => {
        if (task) {
          task.comment_count += 1;
          return task.save();
        } else {
          logger.error("Task not found.")
          throw new Error("Task not found.");
        }
      })
      .then((data) => {
        return Project.findByPk(comment.project_id);
      })
      .then((project) => {
        project.comment_count += 1;
        return project.save();
      })
      .then((data) => {
        res.send(data);
      })
      .catch((err) => {
        console.error("Error creating comment:", err);
        logger.error(err.message || "Some error occurred while creating the Comment.")
        res.status(500).send({
          message: err.message || "Some error occurred while creating the Comment.",
        });
      });
  });
};

exports.update = (req, res) => {
  authJwt.verifyToken(req, res, () => {
    const authenticatedUsername = req.userId;
    const id = req.params.id;

    Comment.update(req.body, {
      where: { id: id, username: authenticatedUsername },
    })
      .then((num) => {
        if (num == 1) {
          res.send({
            message: "Comment was updated successfully.",
          });
        } else {
          logger.error(`Cannot update Comment with id=${id}. Maybe Comment was not found or does not belong to the authenticated user.`)
          res.send({
            message: `Cannot update Comment with id=${id}. Maybe Comment was not found or does not belong to the authenticated user.`,
          });
        }
      })
      .catch((err) => {
        console.error("Error updating comment:", err);
        logger.error(`Internal server error while updating Comment with id=${id}.`)
        res.status(500).send({
          message: `Internal server error while updating Comment with id=${id}.`,
        });
      });
  });
};
exports.delete = (req, res) => {
  authJwt.verifyToken(req, res, () => {
    const authenticatedUsername = req.userId;

    const commentId = req.params.id;

    Comment.findByPk(commentId)
      .then((comment) => {
        if (!comment || comment.username !== authenticatedUsername) {
          logger.error(`Comment with id=${commentId} not found or does not belong to the authenticated user.`)
          return res.status(404).send({
            message: `Comment with id=${commentId} not found or does not belong to the authenticated user.`,
          });
        }

        const taskId = comment.task_id;

        Comment.destroy({ where: { id: commentId, username: authenticatedUsername } })
          .then((numDeleted) => {
            if (numDeleted === 1) {
              Task.findByPk(taskId)
                .then((task) => {
                  if (task) {
                    task.comment_count = Math.max(0, task.comment_count - 1);
                    return task.save();
                  } else {
                    logger.error(`Associated Task with id=${taskId} not found.`)
                    return Promise.reject(
                      `Associated Task with id=${taskId} not found.`
                    );
                  }
                })
                .catch((error) => {
                  logger.error(`Comment was deleted, but ${error}`)
                  res.send({
                    message: `Comment was deleted, but ${error}`,
                  });
                });
              Project.findByPk(comment.project_id)
                .then((project) => {
                  if (project) {
                    project.comment_count = Math.max(
                      0,
                      project.comment_count - 1
                    );
                    return project.save();
                  } else {
                    logger.error(`Associated Project with id=${comment.project_id} not found`)
                    return Promise.reject(
                      `Associated Project with id=${comment.project_id} not found`
                    );
                  }
                })
                .then(() => {
                  res.send({
                    message: "Comment was deleted successfully!",
                  });
                });
            } else {
              logger.error(`Cannot delete Comment with id=${commentId}. Maybe Comment was not found...`)
              res.send({
                message: `Cannot delete Comment with id=${commentId}. Maybe Comment was not found...`,
              });
            }
          })
          .catch((err) => {
            console.error(err);
            logger.error(`Error occurred while deleting Comment with id=${commentId}.`)
            res.status(500).send({
              message: `Error occurred while deleting Comment with id=${commentId}.`,
            });
          });
      })
      .catch((err) => {
        console.error(err);
        res.status(500).send({
          message: `Error occurred while finding Comment with id=${commentId}.`,
        });
      });
  });
};
exports.findOne = (req, res) => {
  authJwt.verifyToken(req, res, () => {

    const authenticatedUsername = req.userId;

    const id = req.params.id;

    Comment.findByPk(id)
      .then((data) => {
        if (data && data.username === authenticatedUsername) {
          res.send(data);
        } else {
          logger.error(`Cannot find Comment with id=${id} or it does not belong to the authenticated user.`)
          res.status(404).send({
            message: `Cannot find Comment with id=${id} or it does not belong to the authenticated user.`,
          });
        }
      })
      .catch((err) => {
        logger.error("Error retrieving Comment with id=" + id)
        res.status(500).send({
          message: "Error retrieving Comment with id=" + id,
        });
      });
  });
};
