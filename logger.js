const { createLogger, format, transports } = require("winston");

const logger = createLogger({
  transports: [
    new transports.Console(),
    new transports.File({
      filename: "logs.log",
    }),
  ],
  format: format.combine(
    format.timestamp({ format: "YYYY-MM-DD HH:mm:ss" }),
    format.json(),
    format.prettyPrint(),
    format.colorize()
  ),
});

module.exports = logger;
