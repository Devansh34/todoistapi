const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const logger = require("../logger.js");
const User = db.user;


verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];
  if (!token) {
    logger.error("No token provided")
    return res.status(403).send({
      message: "No token provided",
    });
  }
  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      logger.error("Unauthorized")
      return res.status(401).send({
        message: "Unauthorized!",
      });
    }
    req.userId = decoded.id;
    next();
  });
};

const authJwt = {
  verifyToken: verifyToken,
};
module.exports = authJwt;
