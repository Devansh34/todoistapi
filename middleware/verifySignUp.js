const db =require("../models");
const User=db.user;
const logger =require("../logger")

checkDuplicateUsernameOrEmail = (req,res,next) => {
    User.findOne({
        where:{
            username:req.body.username
        }
    }).then(user => {
        if(user){
            const message="Failed! Username is already in use!"
            res.status(400).send({
                message:message
            })
            logger.error(message)
            return ;
        }
        User.findOne({
            where:{
                email:req.body.email
            }
        }).then(user => {
            if(user){
                const message="Failed! Email is already in use"
                res.status(400).send({
                    message:message
                })
                logger.error(message)

                return ;
            }
            next();
        })
    })
}

const verifySignUp={checkDuplicateUsernameOrEmail: checkDuplicateUsernameOrEmail};

module.exports=verifySignUp