module.exports = (sequelize, Sequelize) => {
  const Comment = sequelize.define("comment", {
    id: {
      type: Sequelize.UUID,
      primaryKey: true,
      defaultValue: Sequelize.UUIDV4,
    },
    task_id: {
      type: Sequelize.UUID,
      allowNull:false,
      references: {
        model: 'tasks', 
        key: 'id',
      },},
    project_id: {
      type: Sequelize.UUID,
      allowNull:false,
      references: {
        model: 'projects',
        key: 'id',
      },
    },
    posted_at: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    },
    content: {
      type: Sequelize.STRING,
    },
    attachment: {
      type: Sequelize.JSONB,
      defaultValue: null,
    },
    username: {
      type: Sequelize.STRING,
      allowNull:false,
      references: {
        model: 'users', 
        key: 'username',
      },},
  });

  return Comment;
};
