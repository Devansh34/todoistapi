const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle,
  },
});

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.project = require("./project.model.js")(sequelize, Sequelize);
db.task = require("./task.model.js")(sequelize, Sequelize);
db.comment = require("./comment.model.js")(sequelize, Sequelize);
db.label=require("./label.model.js")(sequelize,Sequelize)
db.user=require("./user.model.js")(sequelize,Sequelize);

db.user.hasMany(db.project, { foreignKey: "username" });
db.project.belongsTo(db.user, { foreignKey: "username" });

db.user.hasMany(db.task, { foreignKey: "username" });
db.task.belongsTo(db.user, { foreignKey: "username" });

db.user.hasMany(db.comment, { foreignKey: "username" });
db.comment.belongsTo(db.user, { foreignKey: "username" });

db.user.hasMany(db.label, { foreignKey: "username" });
db.label.belongsTo(db.user, { foreignKey: "username" });

db.project.hasMany(db.task, { foreignKey: "project_id" });
db.task.belongsTo(db.project, { foreignKey: "project_id" });

db.task.hasMany(db.comment, { foreignKey: "task_id" });
db.comment.belongsTo(db.task, { foreignKey: "task_id" });

db.project.hasMany(db.comment, { foreignKey: "project_id" });
db.comment.belongsTo(db.project, { foreignKey: "project_id" });


module.exports = db;
