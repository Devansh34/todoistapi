module.exports = (sequelize, Sequelize) => {
  const Label = sequelize.define("label", {
    id: {
      type: Sequelize.UUID,
      primaryKey: true,
      defaultValue: Sequelize.UUIDV4,
    },
    name: {
      type: Sequelize.STRING,
      unique: true,
    },
    color: {
      type: Sequelize.STRING,
    },
    order: {
      type: Sequelize.INTEGER,
    },
    is_favorite: {
      type: Sequelize.BOOLEAN,
    },
    username: {
      type: Sequelize.STRING,
      allowNull: false,
      references: {
        model: "users", 
        key: "username",
      },
    },
  });
  return Label;
};
