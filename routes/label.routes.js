const { createLabelSchema, deleteLabelSchema } = require("../Validations/userValidation.js");
const validation = require("../middleware/validationMiddleware.js");

module.exports = (app) => {
    const labels = require("../controllers/label.controller.js");
    var router = require("express").Router();
    router.get("/", labels.findAll);
    router.post("/",validation(createLabelSchema),labels.create);
    router.put("/:id",validation(deleteLabelSchema),labels.update);
    router.delete("/:id",labels.delete);
    router.get("/:id",labels.findOne);
    app.use("/api/labels", router);
  };
  